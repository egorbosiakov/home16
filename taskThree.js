/////////////////----1
const obj = {
  age: 20,
  name: "olga",
  aav: "feffd",
};
let one = "";
let cont = "";
for (key in obj) {
  if (typeof obj[key] === "string") {
    if (one === "") {
      one = obj[key];
    }
    cont = cont + obj[key];
    delete obj[key];
  }
}
//////////////------2
obj[one] = cont;
///////////-------3
const cop = { ...obj };
const presence = "aav" in cop;
console.log(presence);
/////////////-------4
arra = [12, 45, 2];
arrb = [2, 5, 5];
const first = arra[0] + arrb[0];
const last = arra[arra.length - 1] + arrb[arrb.length - 1];
const arrc = [first, ...arra, ...arrb, last];

///////////--------5
let minIndex = 0;
let maxIndex = 0;
for (key in arrc) {
  if (arrc[minIndex] > arrc[key]) {
    minIndex = Number(key);
  }
  if (arrc[maxIndex] < arrc[key]) {
    maxIndex = Number(key);
  }
}
[arrc[minIndex], arrc[maxIndex]] = [arrc[maxIndex], arrc[minIndex]];
