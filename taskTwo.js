///////----1
//
const age = 2020;
if (age > 2021) {
  console.log("вас ущё нету");
} else if (age > 2010 && age < 2021) {
  console.log("вы зуммер");
} else {
  console.log("вы бумер");
}
//////////////-------2
//
switch (age) {
  case 2021: {
    console.log("День добрый");
    break;
  }
  case 2020: {
    console.log("День добрый из прошлого");
    break;
  }
  case 2022: {
    console.log("День добрый из будущего");
    break;
  }
  default: {
    console.log("Привет");
  }
}
////////////-----3
//
let str = "q2sw 1 dd3";
let numFromStr = "";
for (let i = 0; i < str.length; i++) {
  if (!str[i].includes(" ")) {
    !isNaN(Number(str[i]))
      ? (numFromStr = numFromStr + (Number(str[i]) + i))
      : (numFromStr = numFromStr + str[i].toUpperCase());
  } else {
    numFromStr = numFromStr + str[i];
  }
}
str = numFromStr;
console.log(str);

////////------4
let i = 0;
let numberOne = 0;
while (true) {
  let accidNumber = Math.floor(Math.random() * 4);
  console.log(accidNumber);
  if (accidNumber === 3 && numberOne === 3) {
    break;
  } else {
    numberOne = accidNumber;
  }
  i = i + 1;
}
